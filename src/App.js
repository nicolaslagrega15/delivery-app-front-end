import React, { useEffect, useContext, useState } from 'react';
import { BrowserRouter, Redirect, Switch, Route, useLocation } from "react-router-dom";

import ScrollToTop from './components/utils/ScrollToTop'

import PrivateRoute from './wrappers/PrivateRoute'
import UserWrapper from './wrappers/UserWrapper'
import CartWrapper from './wrappers/CartWrapper'

import { UserContext } from './wrappers/UserWrapper';

import Home from './pages/Home'
import SignIn from './pages/SignIn'
import SignUp from './pages/SignUp'
import confirmYourEmail from './pages/ConfirmYourEmail'
import Store from './pages/Store'
import CheckOut from './pages/CheckOut'
import Orders from './pages/Orders'
import Order from './pages/Order'
import MyProfile from './pages/MyProfile'
import Addresses from './pages/Addresses'
import UserInfo from './pages/UserInfo'
import Deliveries from './pages/Deliveries'
import Delivery from './pages/Delivery';
import Map from './pages/Map';

import GoogleAuthCallback from './components/googleAuth/GoogleAuthCallback'
import NavBar from './components/navBar/NavBar'

function App() {
  const [showNavBar, setShowNavBar] = useState(false)

  return (
    <div className="App">
      <UserWrapper>
        <CartWrapper>
          <BrowserRouter>
            <ScrollToTop />
            {showNavBar && <NavBar />}
            <Content setShowNavBar={setShowNavBar} />
          </BrowserRouter>
        </CartWrapper>
      </UserWrapper>

    </div>
  );
}

const Content = ({ setShowNavBar }) => {
  const { pathname } = useLocation();
  const { user } = useContext(UserContext)

  useEffect(() => {
    if (pathname !== "/", pathname !== "/sign-in" && pathname !== "/sign-up" && pathname !== "/checkout") setShowNavBar(true)
    else setShowNavBar(false)
  }, [pathname])

  return (
    <Switch>
      <Route exact path="/" component={SignIn} />
      <Route exact path="/sign-in" component={SignIn} />
      <Route exact path="/sign-up" component={SignUp} />
      <Route exact path="/confirm-your-email" component={confirmYourEmail} />
      <Route exact path="/auth/google/callback" component={GoogleAuthCallback} />

      <PrivateRoute exact path="/store" component={Store} role="authenticated" />
      <PrivateRoute exact path="/checkout" component={CheckOut} role="authenticated" />

      <PrivateRoute exact path="/my-profile" component={MyProfile} />
      <PrivateRoute exact path="/my-orders" component={Orders} role="authenticated" />
      <PrivateRoute exact path="/my-orders/order/:id" component={Order} role="authenticated" />
      <PrivateRoute exact path="/my-addresses" component={Addresses} role="authenticated" />
      <PrivateRoute exact path="/my-info" component={UserInfo} />

      <PrivateRoute exact path="/my-deliveries" component={Deliveries} role="deliveryman" />
      <PrivateRoute exact path="/my-deliveries/delivery/:id" component={Delivery} role="deliveryman" />
      <PrivateRoute exact path="/map/:id" component={Map} role="deliveryman" />
      
      <Redirect to={"/"} />
    </Switch>
  )
};

export default App;
