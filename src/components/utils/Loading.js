import React from 'react';
import { Spinner } from 'react-bootstrap'

const Loading = () => {
    return (
        <div className="vh-100 d-flex justify-content-center d-flex align-items-center">
            <Spinner animation="border" variant="primary" />
        </div>
    )
}

export default Loading