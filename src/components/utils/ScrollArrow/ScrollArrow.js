
import React, { useState, useEffect } from 'react';
//import { FaArrowCircleUp } from 'react-icons/fa';
import arrowTop from '../../../media/img/icons/subir.svg'
import './scrollArrow.css';
import { Link, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

const ScrollArrow = () => {

  const [showScroll, setShowScroll] = useState(true)

  /* const checkScrollTop = () => {
    if (!showScroll && window.pageYOffset > 400) {
      setShowScroll(true)
    } else if (showScroll && window.pageYOffset <= 400) {
      setShowScroll(false)
    }
  }; */

  /* 
    useEffect(() => {
      window.addEventListener('scroll', checkScrollTop)
    }, [])
  
   */


  return (
    <div className="scrollTop rounded-circle p-2 bg-light"  style={{opacity: "0.6"}}  onClick={() => scroll.scrollToTop()}>
      <img alt="arrow icon" src={arrowTop} width="30"  style={{ display: showScroll ? 'flex' : 'none' }} />
    </div>
  )

}

export default ScrollArrow