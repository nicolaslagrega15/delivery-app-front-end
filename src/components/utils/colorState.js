export function setColorState(state) {
    switch (state) {
        case "pending":
            return "warning"
        case "transit":
            return "primary"
        case "finalized":
            return "success"
        case "success":
            return "success"
        case "canceled":
            return "danger"
        default:
            return "warning"
    }
}