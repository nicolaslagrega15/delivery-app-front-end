import React from 'react'
import { Button, Col, Row } from 'react-bootstrap';
import xIcon from './../../media/img/icons/letra-x.svg'

const InfoMarkerOrder = ({ infoMarker, updateStatusOrder, showInfoMarker }) => {
    return (
        <div className="p-4 fixed-bottom overflow-auto" style={{ backgroundColor: "white", height: "35%" }}>
            <img src={xIcon} alt="icon close cart" width="17px" onClick={() => showInfoMarker()} style={{ position: "absolute", top: "15px", right: "12px", marginRight: "10px", cursor: "pointer" }}></img>
            <h3 className="d-block">{infoMarker.address.companyName}</h3>
            <h4 className="d-block">ID - {infoMarker.id}</h4>
            <span>{infoMarker.address.address}, {infoMarker.address.secondAddress}</span>
            <span className="d-block">{infoMarker.address.postalCode}</span>
            <span>{infoMarker.address.name} - <a href={"tel:+34" + infoMarker.address.phoneNumber}>{infoMarker.address.phoneNumber}</a></span>
            {infoMarker.paymentMethod === "cash" && <h5>Cobrar efectivo {infoMarker.amount}€</h5>}
            {infoMarker.status === "transit"
                ? <Row>
                    <Col className="mt-3" xs={12} md={6}>
                        <Button variant="primary" onClick={() => { updateStatusOrder(infoMarker.id, "finalized"); showInfoMarker(); }} block> ENTREGADO</Button>
                    </Col>
                    <Col className="mt-3" xs={12} md={6}>
                        <Button variant="danger" onClick={() => { updateStatusOrder(infoMarker.id, "cancelled"); showInfoMarker(); }} block>CANCELAR</Button>
                    </Col>
                </Row>
                :
                <Row className="mt-3">
                    <Col><span className="font-weight-bolder">Status: {infoMarker.status}</span></Col>
                </Row>
            }
            <Row className="mt-3">
                <Col xs={12}>
                    <span className="font-weight-bolder">Resumen: </span>
                </Col>
                <Col xs={12} className="mb-3">
                    <Row className="text-left">
                        <Col xs={4}><span className="font-weight-bolder">ID</span></Col>
                        <Col xs={4}><span className="font-weight-bolder">Nombre</span></Col>
                        <Col xs={4}><span className="font-weight-bolder">Cantidad</span></Col>
                    </Row>
                </Col>
                <Col xs={12} className="overflow-auto">
                    {infoMarker.orderproducts.map(product =>
                        <Row key={product.id} className="text-left">
                            <Col xs={4}><span className="font-weight-bolder">{product.product}</span></Col>
                            <Col xs={4}><span className="font-weight-bolder">{product.name}</span></Col>
                            <Col xs={4}><span className="font-weight-bolder">{product.quantity}</span></Col>
                        </Row>
                    )}
                </Col>

            </Row>
        </div>
    )
}

export default InfoMarkerOrder