import React, { useEffect, useContext } from "react"
import { useLocation, useHistory } from "react-router-dom"
import { loginGoogleAuthCallback } from "../../api/requests";

import Loading from '../utils/Loading'

import { UserContext } from "../../wrappers/UserWrapper";

const GoogleAuthCallback = () => {

    const STORAGE_USER_TOKEN = process.env.REACT_APP_STORAGE_USER_TOKEN;

    const { loginGoogle, logout } = useContext(UserContext)

    const location = useLocation()
    const history = useHistory();

    useEffect(() => {
        localStorage.removeItem(STORAGE_USER_TOKEN)
        if (!location) {
            return null
        }

        (async function () {
            logout()
            const { search } = location
            const { data } = await loginGoogleAuthCallback(search)
            if (data) {
                await loginGoogle(data)
                if (data.user.role.type === "authenticated") history.push('/store')
                else history.push('/my-profile')
            }
        })();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location])

    return <Loading />
}

export default GoogleAuthCallback