import React from "react"

import { Col, Row } from "react-bootstrap";

import Loading from '../utils/Loading'
import Product from './Product'

const Products = ({products}) => {

    if (!products) return <Loading />

    if (products.length === 0) return <Col xs={12} className="p-4 text-center"><span >No hay productos</span></Col>

    return (
        <Row>
            {products.map((product) => (
                <Col className="border" key={product.id} xs={6} md={6} lg={4} xl={3} >
                    <Product product={product} />
                </Col>
            ))}
        </Row>
    )
}

export default Products