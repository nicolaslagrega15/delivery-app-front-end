import React, { useState, useContext } from "react";
import { NavLink } from "react-router-dom";

import { CartContext } from './../../wrappers/CartWrapper'

import { Button, Col, Row, InputGroup, FormControl } from 'react-bootstrap';
import './products.css'

import productDefaultImage from '../../media/img/default-images/product-default.jpg'

const Product = ({ product }) => {
    const { addItem } = useContext(CartContext)

    const API_URL = process.env.REACT_APP_API_URL;
    const [quantity, setQuantity] = useState(1)

    function sumItem() {
        setQuantity(quantity + 1)
    }

    function resItem() {
        if (quantity > 1) {
            setQuantity(quantity - 1)
        }
    }
    return (
        <Row className="pb-4" >
            <Col className="text-left p-0" xs={12}>
                <NavLink className="text-decoration-none" to={"/store/product/" + product.id}>
                    <div className="mb-3 products-image" style={{ backgroundImage: `url(${product.image[0] ? API_URL + product.image[0].url : productDefaultImage})`, backgroundSize: "cover", backgroundPosition: "center" }} ></div>
                    <p className="mb-0 pl-3 text-dark text-left" >{product.name}</p>
                    <p className="pl-3 text-secondary font-weight-bold ">
                        {product.price} €
                    </p>
                </NavLink>
            </Col>
            <Col className="p-3 d-flex align-items-center text-center" xs={12} md={6} >
                <InputGroup>
                    <InputGroup.Prepend>
                        <Button variant="outline-dark" onClick={() => resItem()}>-</Button>
                    </InputGroup.Prepend>
                    <FormControl aria-describedby="basic-addon1" className="text-center" value={quantity} readOnly />
                    <InputGroup.Append>
                        <Button variant="outline-dark" onClick={() => sumItem()} >+</Button>
                    </InputGroup.Append>
                </InputGroup>
            </Col>
            <Col className="p-3 d-flex align-items-center text-center" xs={12} md={6}>
                <Button className="mx-auto" variant="dark" onClick={() => addItem(product.id, quantity)} block>Añadir</Button>
            </Col>

        </Row>
    )
};

export default Product