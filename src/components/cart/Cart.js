import React, { useContext} from "react";
import { Row, Col, Button, InputGroup, FormControl } from 'react-bootstrap';
import './cart.css'

import { NavLink } from "react-router-dom";

import { CartContext } from './../../wrappers/CartWrapper'

import Loading from "../utils/Loading";

import xIcon from './../../media/img/icons/letra-x.svg'
import productDefault from '../../media/img/default-images/product-default.jpg'


const ShoppingCart = () => {

    const { showCart, displayCart, totalItems, totalPrice } = useContext(CartContext)

    return (
        <React.Fragment>
            { showCart &&
                <div className="m-0 p-4 cart-box">
                    <Row>
                        <Col xs={12}>
                            <p className="text-center" style={{ fontSize: "0.9rem" }}>BOLSA {totalItems} ARTÍCULOS</p>
                            <img src={xIcon} alt="icon close cart" width="17px" onClick={() => displayCart(false)} style={{ position: "absolute", top: "0", right: "0", marginRight: "10px", cursor: "pointer" }}></img>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} className="items-container p-3 text-center" style={{ height: "60vh", overflow: "auto" }}>
                            {totalItems === 0 ? <span className="font-weight-bold align-middle">Tú bolsa está vacía</span> : <Products />}
                        </Col>
                        <Col xs={12} className="my-4 d-flex justify-content-between" style={{ fontSize: "1.2rem" }}>
                            <span>Subtotal</span>
                            <span>€{totalPrice}</span>
                        </Col>
                        <Col className="mb-2" xs={12} >
                            <PayButton />
                        </Col>
                    </Row>
                </div>}

        </React.Fragment>
    )
}

const Products = () => {
    const API_URL = process.env.REACT_APP_API_URL;
    const { removeItem, sumItem, resItem, cartItemsMerged } = useContext(CartContext)

    return (
        <React.Fragment>
            {cartItemsMerged ?
                cartItemsMerged.map((item, index) => (
                    <Row key={index} className="items mb-4 py-4 px-3">
                        <Col xs={6} key={item.id} alt={item.name} style={{ backgroundImage: `url(${item.image[0] ? API_URL + item.image[0].url : productDefault})`, backgroundSize: "cover", backgroundPosition: "center" }}>                                </Col>
                        <Col xs={6} className="p-0 pl-3 mb-3">
                            <img src={xIcon} width="12px" onClick={() => removeItem(item.id)} alt="icon close cart" style={{ position: "absolute", top: "-12px", right: "-8px", cursor: "pointer" }}></img>
                            <span style={{ fontWeight: "600", color: "#000", fontSize: "1.1rem" }} className="text-left d-block">{item.name}</span>
                            <span className="text-left text-dark d-block mb-5" style={{ fontSize: "1.1rem" }}>€{item.price}</span>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <Button variant="outline-dark" onClick={() => resItem(item.id)} >-</Button>
                                </InputGroup.Prepend>
                                <FormControl aria-describedby="basic-addon1" className="text-center" value={item.quantity} readOnly />
                                <InputGroup.Append>
                                    <Button variant="outline-dark" onClick={() => sumItem(item.id)} >+</Button>
                                </InputGroup.Append>
                            </InputGroup>
                        </Col>
                    </Row>
                ))
                :
                <Loading />
            }
        </React.Fragment>
    )
}

export const PayButton = () => {
    const { displayCart, totalItems } = useContext(CartContext)
    return (
        <NavLink to={"/checkout"}>
            <Button onClick={() => displayCart(false)} className="rounded-pill" variant="dark" size="lg" block disabled={totalItems === 0}>
                Continuar con el pago
            </Button>
        </NavLink>
    )
}

export default ShoppingCart