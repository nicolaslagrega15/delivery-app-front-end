import React from 'react'
import { Container, Button, Jumbotron } from 'react-bootstrap';
import { NavLink } from "react-router-dom";

const OrderCompletedJumbotron = () => {
    return (
        <Jumbotron fluid className="bg-secondary">
            <Container className="text-center" >
                <h2 className="text-light">Gracias por realizar tu pedido</h2>
                <p className="text-light">
                    Prepararemos tú pedido cuantos antes, recibirás un email cuando el pedido salga de nuestros almacenes
                    </p>
                <NavLink to={"/my-orders"}>
                    <Button className="rounded-0" variant="warning">MIS PEDIDOS</Button>
                </NavLink>
            </Container>
        </Jumbotron>
    )
}

export default OrderCompletedJumbotron