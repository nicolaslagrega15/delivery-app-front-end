import React from 'react'
import { Row, Col, Form } from 'react-bootstrap';
import { Link, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

import paypalIcon from '../../../media/img/icons/paypal.svg'
import cashIcon from '../../../media/img/icons/pago-en-efectivo.svg'
import cardIcon from '../../../media/img/icons/tarjeta-de-credito.svg'

const PaymentMethods = ({ setPaymentMethod }) => {
    const handleChange = async (event) => {
        setPaymentMethod(event.target.id)
        scroll.scrollToBottom()
    }

    return (
        <Form.Group as={Row} className="p-3">
            <Col sm={10}>
                <div className="mb-2">
                    <Form.Check className="d-inline"
                        type="radio"
                        label="Contra reembolso"
                        name="formHorizontalRadios"
                        id="cashPayment"
                        onChange={handleChange}
                    />
                    <img className="d-inline mx-2" src={cashIcon} width="30px" />
                </div>
                <div className="mb-2">
                    <Form.Check className="d-inline"
                        type="radio"
                        label="Tarjeta de crédito"
                        name="formHorizontalRadios"
                        id="cardPayment"
                        onChange={handleChange}
                    />
                    <img className="d-inline mx-2" src={cardIcon} width="30px" />
                </div>
                <div className="mb-2">
                    <Form.Check className="d-inline"
                        type="radio"
                        label="Paypal"
                        name="formHorizontalRadios"
                        id=""
                        onChange={handleChange}
                    />
                    <img className="d-inline mx-2" src={paypalIcon} width="30px" />
                </div>
            </Col>
        </Form.Group>
    )
}

export default PaymentMethods