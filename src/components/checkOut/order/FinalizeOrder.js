import React from 'react'
import { Button } from 'react-bootstrap';

import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";

import CheckoutForm from "../../checkOut/checkOutForm/CheckoutForm";

const FinalizeOrder = ({ paymentMethod, createOrderCustomer }) => {

    // Make sure to call loadStripe outside of a component’s render to avoid
    // recreating the Stripe object on every render.
    // loadStripe is initialized with a fake API key.
    // Sign in to see examples pre-filled with your key.
    const STRIPE_PUBLIC_KEY = process.env.REACT_APP_STRIPE_PUBLIC_KEY
    const promise = loadStripe(STRIPE_PUBLIC_KEY);

    return (
        <React.Fragment>
            { paymentMethod == "cardPayment" &&
                < Elements stripe={promise}  >
                    <CheckoutForm createOrderCustomer={createOrderCustomer} />
                </Elements >
            }

            { paymentMethod == "cashPayment" &&
                <div className="w-100 d-flex justify-content-center mb-3">
                    <Button onClick={() => createOrderCustomer()} className="rounded-0" variant="warning">Finalizar compra</Button>
                </div>
            }
        </React.Fragment >
    )
}

export default FinalizeOrder