import React, { useContext } from 'react'
import { CartContext } from '../../../wrappers/CartWrapper'
import { Row, Col } from 'react-bootstrap';
import Loading from '../../utils/Loading';
import imageProductDefault from '../../../media/img/default-images/product-default.jpg'

const ProductsResume = () => {
    const { cartItemsMerged } = useContext(CartContext)
    const API_URL = process.env.REACT_APP_API_URL;
    return (
        <React.Fragment>
            {cartItemsMerged ?
                <div className="products-resume p-3 border-right border-left border-bottom">
                    {cartItemsMerged.map((item, index) => (
                        <Row key={index} className="mb-2 py-2 px-3 border-bottom">
                            <Col xs={4} lg={4} key={item.id} style={{ backgroundImage: `url(${item.image[0] ? API_URL + item.image[0].url : imageProductDefault})`, backgroundSize: "cover", backgroundPosition: "center" }}></Col>
                            <Col xs={8} lg={8} className="p-0 pl-4 mb-2">
                                <span className="text-left d-block" style={{ fontWeight: "600", color: "#000", fontSize: "0.8rem" }}>{item.name}</span>
                                <span className="text-left d-block text-secondary" style={{ fontSize: "0.9rem" }}>Cant.:{item.quantity}  -  €{item.price}</span>
                                <span className="text-left d-block text-dark " style={{ fontSize: "0.8rem" }}>€{(item.quantity * item.price).toFixed(2)}</span>
                            </Col>
                        </Row>
                    ))}
                </div>
                :
                <Loading />
            }
        </React.Fragment>

    )
}

export default ProductsResume