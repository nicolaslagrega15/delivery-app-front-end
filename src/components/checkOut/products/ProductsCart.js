import React, { useContext } from 'react'
import { CartContext } from '../../../wrappers/CartWrapper'
import { Col } from 'react-bootstrap';
import ProductsResume from './ProductsResume';

const ProductsCart = () => {
    const { totalPrice } = useContext(CartContext)
    return (
        <Col xs={{ order: 'first', span: 12 }} lg={{ order: 'last', span: 3 }} className="px-3 mb-3">
            <h2 className="p-3 m-0 text-left bg-secondary text-light font-weight-bold" style={{ fontSize: "1rem" }} >PRODUCTOS EN TU CESTA</h2>
            <div className="p-3 border-right border-left ">
                <div className="justify-content-between d-flex">
                    <span style={{ fontSize: "0.9rem" }}>Subtotal</span>
                    <span style={{ fontSize: "0.9rem" }}>{totalPrice} €</span>
                </div>
                <div className="justify-content-between d-flex mb-3">
                    <span style={{ fontSize: "0.9rem" }}>Gastos de envío estimados</span>
                    <span style={{ fontSize: "0.9rem" }}>0,00 €</span>
                </div>
                <div className="justify-content-between d-flex">
                    <span className="font-weight-bold">Total</span>
                    <span className="font-weight-bold">{totalPrice} €</span>
                </div>
                <hr className="horizontal-divider"></hr>
            </div>
            <ProductsResume />
        </Col>
    )
}

export default ProductsCart