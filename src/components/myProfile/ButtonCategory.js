import React from "react";
import { NavLink } from "react-router-dom"
import { Card, Col } from "react-bootstrap";

const ButtonCategory = ({ title, link, text }) => {
    return (
        <Col md={4}>
            <NavLink to={link}>
                <Card className="mt-4 border border-primary" >
                    <Card.Body>
                        <Card.Title className="text-dark">{title}</Card.Title>
                        {/* <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle> */}
                        <Card.Text>
                            {text}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </NavLink>
        </Col>
    )
}

export default ButtonCategory