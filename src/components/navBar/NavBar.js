import React, {/*  useState,  */useContext } from "react";
import { NavLink } from "react-router-dom";

import { Navbar, Nav, NavDropdown } from 'react-bootstrap';

import shoppingBag from './../../media/img/icons/bolsa-de-la-compra-dark.svg'
import userIcon from './../../media/img/icons/usuario.svg'
import logoutIcon from './../../media/img/icons/logout.svg'

import { CartContext } from './../../wrappers/CartWrapper'
import Cart from './../cart/Cart'

import { UserContext } from "../../wrappers/UserWrapper";

const NavBar = () => {
    const { displayCart, totalItems } = useContext(CartContext)
    const { user, logout } = useContext(UserContext)

    if (!user) return null

    return (
        <React.Fragment>
            <Navbar className="shadow-sm " collapseOnSelect expand="false" bg="light" variant="light" fixed="top" >
                <Cart />
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
                <div>
                    <NavLink to={"/my-profile"}>
                        <img alt="user icon" src={userIcon} width="30" className="pl-2 " role="button" />{''}
                    </NavLink>
                    {user.role.type === "authenticated" &&
                        <div className="d-inline" onClick={() => displayCart(true)} role="button">
                            <img alt="shopping bag icon" src={shoppingBag} width="35" className="pl-2" />{''}
                            {totalItems > 9 ?
                                <span className="position-absolute" style={{ top: "21px", right: "21px", fontSize: "0.8rem" }}>9+</span>
                                :
                                <span className="position-absolute" style={{ top: "19px", right: "26px" }}>{totalItems}</span>
                            }
                        </div>
                    }
                </div>

                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        {user.role.type === "authenticated" ? <UserNav /> : <DeliveryManNav />}
                    </Nav>
                    <Nav className="mr-auto">
                        <NavLink to={"/sign-in"} onClick={logout} style={{ color: "#000" }}>
                            <img alt="user icon" src={logoutIcon} width="25" className="pl-2 " role="button" />{'Logout'}
                        </NavLink>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            <div style={{ marginBottom: "57px" }}></div>
        </React.Fragment>
    )
}

const UserNav = () => {
    return (
        <>
            <NavLink className="nav-link" to={"/store"}>Inicio</NavLink>
            <NavDropdown title="Mi perfil" id="collasible-nav-dropdown">
                <NavLink className="dropdown-item" to={"/my-orders"}> Mis pedidos</NavLink>
                <NavLink className="dropdown-item" to={"/my-addresses"}> Direcciones </NavLink>
                <NavLink className="dropdown-item" to={"/my-info"}>Inicio de sesión y seguridad</NavLink>
            </NavDropdown>
        </>
    )
}

const DeliveryManNav = () => {
    return (
        <>
            <NavLink className="nav-link" to={"/my-deliveries"}>Inicio</NavLink>
            <NavDropdown title="Mi perfil" id="collasible-nav-dropdown">
                <NavLink className="dropdown-item" to={"/my-deliveries"}>Mis entregas</NavLink>
                <NavLink className="dropdown-item" to={"/my-info"}>Inicio de sesión y seguridad</NavLink>
            </NavDropdown>
        </>
    )
}

export default NavBar