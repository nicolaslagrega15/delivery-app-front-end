import React, { useState, useEffect } from 'react'
import { Col, Button, Form } from 'react-bootstrap';
import { createAddress, updateAddress } from "../../api/requests";
import AddressMapCoords from './AddressMapCoords';

const AddressForm = ({ refreshAddresses, addressUpdate, setAddressUpdate, setShowForm }) => {

    const [validated, setValidated] = useState(false);
    const [latLng, setLatlng] = useState(null)

    const handleSubmit = async (event) => {
        event.preventDefault();
        event.stopPropagation();
        const form = event.currentTarget;
        if (form.checkValidity() === false) {

        } else {
            const name = event.target.name.value
            const surnames = event.target.surnames.value
            const address = event.target.address.value
            const secondAddress = event.target.secondAddress.value
            const city = event.target.city.value
            const country = event.target.country.value
            const province = event.target.province.value
            const postalCode = event.target.postalCode.value
            const phoneNumber = event.target.phoneNumber.value

            const  lat = latLng.toString().substring(1).split(",")[0]
            const  lng = latLng.toString().substring(1).split(",")[1].substring(0,latLng.toString().substring(1).split(",")[1].length-1)

            if (addressUpdate) {
                const { data } = await updateAddress(addressUpdate.id, { name, surnames, address, secondAddress, city, province, country, postalCode, phoneNumber, lat, lng })
                if (data) await refreshAddresses()
                setAddressUpdate(null)
            } else {
                const { data } = await createAddress({ name, surnames, address, secondAddress, city, province, country, postalCode, phoneNumber, lat, lng })
                if (data) await refreshAddresses()
            }
        }
        setValidated(true);
    }
    return (
        <>
            <Form noValidate validated={validated} onSubmit={handleSubmit} className="border p-3 w-auto">
                <Form.Row>
                    <Form.Group as={Col} md="6" controlId="name">
                        <Form.Control
                            required
                            type="text"
                            placeholder="Nombre"
                            defaultValue={addressUpdate ? addressUpdate.name : ""}
                        />
                        <Form.Control.Feedback type="invalid">Escribe tu nombre.</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="6" controlId="surnames">
                        <Form.Control
                            required
                            type="text"
                            placeholder="Apellidos"
                            defaultValue={addressUpdate ? addressUpdate.surnames : ""}
                        />
                        <Form.Control.Feedback type="invalid">Escribe tus apellidos.</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="12" controlId="address">
                        <Form.Control
                            required
                            type="text"
                            placeholder="Calle y número"
                            defaultValue={addressUpdate ? addressUpdate.address : ""}
                        />
                        <Form.Control.Feedback type="invalid">Escribe una dirección válida</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="12" controlId="secondAddress">
                        <Form.Control
                            type="text"
                            placeholder="Añadir apratamento, habitación, bloque... (OPCIONAL)"
                            defaultValue={addressUpdate ? addressUpdate.secondAddress : ""}
                        />
                    </Form.Group>
                </Form.Row>
                <Form.Row>
                    <Form.Group as={Col} md="3" controlId="country">
                        <Form.Control type="text" placeholder="País" required defaultValue={addressUpdate ? addressUpdate.country : ""} />
                        <Form.Control.Feedback type="invalid">
                            Este campo es obligatorio.
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="3" controlId="province">
                        <Form.Control type="text" placeholder="Provincia" required defaultValue={addressUpdate ? addressUpdate.province : ""} />
                        <Form.Control.Feedback type="invalid">
                            Introduce una província válida.
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="6" controlId="city">
                        <Form.Control type="text" placeholder="Ciudad" required defaultValue={addressUpdate ? addressUpdate.city : ""} />
                        <Form.Control.Feedback type="invalid">
                            Escribe una ciudad válida.
                        </Form.Control.Feedback>
                    </Form.Group>
                </Form.Row>
                <Form.Row>
                    <Form.Group as={Col} md="3" controlId="postalCode">
                        <Form.Control type="number" placeholder="Código postal" required defaultValue={addressUpdate ? addressUpdate.postalCode : ""} />
                        <Form.Control.Feedback type="invalid">
                            Introduce tu código postal.
                        </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group as={Col} md="6" controlId="phoneNumber">
                        <Form.Control type="number" placeholder="Número de teléfono" required defaultValue={addressUpdate ? addressUpdate.phoneNumber : ""} />
                        <Form.Control.Feedback type="invalid">
                            Este campo es obligatorio.
                        </Form.Control.Feedback>
                    </Form.Group>
                </Form.Row>
                <Form.Group>
                    <Form.Check
                        className="text-left"
                        required
                        label="Agree to terms and conditions"
                        feedback="You must agree before submitting."
                    />
                </Form.Group>
                <Button className="rounded-0 m-1" type="submit" variant="dark" >{addressUpdate ? "Actualizar" : "Añadir"} dirección</Button>
                <Button className="rounded-0 m-1" variant="danger" onClick={() => { setShowForm(false); setAddressUpdate(null); }} >Cancelar</Button>

            </Form>
            <AddressMapCoords setLatlng={setLatlng} addressUpdate={addressUpdate} />
        </>

    )
}
export default AddressForm