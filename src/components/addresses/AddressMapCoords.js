import React, { useEffect, useState } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';
import { Container } from 'react-bootstrap';


const AddressesMapCoords = (props) => {

    const [coords, setCoords] = useState({ lat: 39.61943157333034, lng: 2.729006935590834 })

    const mapClicked = (mapProps, map, clickEvent) => {
        setCoords(clickEvent.latLng)
        props.setLatlng(clickEvent.latLng)
    }

    useEffect(() => {
        setCoords({ lat: props.addressUpdate.lat, lng: props.addressUpdate.lng })
    }, [])

    return (
        <Container fluid >
            <Map google={props.google} zoom={12} style={style} initialCenter={coords} onClick={mapClicked}>
                {coords &&
                    <Marker
                        name={'Your position'}
                        position={coords}
                    />
                }
            </Map >
        </Container>
    );
}

const style = {
    width: '90%',
    height: '100%',
    position: 'relative'
}

export default GoogleApiWrapper({
    apiKey: (process.env.REACT_APP_GOOGLE_MAPS_API)
})(AddressesMapCoords)