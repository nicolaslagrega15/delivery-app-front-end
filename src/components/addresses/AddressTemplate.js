import React from 'react'

const AddressTemplate = ({ address }) => {
    return (
        <>
            <span className="font-weight-bold d-block">{address.name} {address.surnames}</span>
            <span className="d-block">{address.address},  {address.secondAddress && address.secondAddress}</span>
            <span className="d-block">{address.country}, {address.province}, {address.city}, {address.postalCode}</span>
            <span className="d-block">{address.phoneNumber}</span>
        </>
    )
}

export default AddressTemplate