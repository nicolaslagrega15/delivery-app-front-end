import react,{useState} from 'react'
import { Container, Row, Col, Button, Form, Modal, Jumbotron } from 'react-bootstrap';

const RemoveAddressButton = ({ id, deleteAddresses }) => {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <Button className="mt-2 rounded-0" onClick={handleShow} variant="danger"  >Eliminar</Button>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>¿Quieres eliminar esta dirreción?</Modal.Title>
                </Modal.Header>
                <Modal.Footer>
                    <Button className="rounded-0" variant="dark" onClick={handleClose}>Cancelar</Button>
                    <Button className="rounded-0" variant="danger" onClick={() => { deleteAddresses(id); handleClose(); }}>Eliminar</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default RemoveAddressButton