import React from 'react'
import { Container, Row, Col, Button, Form, Modal, Jumbotron } from 'react-bootstrap';

import AddressTemplate from './AddressTemplate'
import RemoveAddressButton from './RemoveAddressButton'

const UserAddresses = ({ addresses, deleteAddresses, updateAddresses, setAddressSelected }) => {
    const handleChange = async (event) => {

        setAddressSelected(addresses.find(address => address.id == event.target.value))
    }
    return (
        <Form className="w-100 px-3">
            <Row>
                {addresses.map(address => (
                    <Col className="border p-4" xs={12} md={6} key={address.id}>
                        <Form.Group as={Col} controlId={"address" + address.id} >
                            <Form.Check
                                type="radio"
                                label="Enviar a esta dirrección"
                                name="formHorizontalRadios"
                                id={"address" + address.id}
                                onChange={handleChange}
                                defaultValue={address.id}
                            />
                        </Form.Group>
                        <AddressTemplate address={address} />
                        <Button className="mt-2 mr-2 rounded-0" variant="dark" onClick={() => updateAddresses(address)}>Actualizar</Button>
                        <RemoveAddressButton id={address.id} deleteAddresses={deleteAddresses} />
                    </Col>
                ))}
            </Row>
        </Form >
    )
}

export default UserAddresses