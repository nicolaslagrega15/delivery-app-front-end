import Request from "./request";

export const registerRequest = (body) => Request().post('auth/local/register', body);
export const loginRequest = (body) => Request().post("auth/local", body);
export const loginGoogleAuthCallback = (search) => Request().get(`/auth/google/callback${search}`);
export const getUserMe = () => Request().get("users/me");

export const getProducts = (start, limit) => Request().get("products?_start=" + start + "&_limit=" + limit);
export const getProductById = (id) => Request().get('products/' + id);
export const getTotalProducts = () => Request().get("products/count");
export const getCategories = () => Request().get("categories");

export const getAddressesByCustomer = () => Request().get('addresses/customer');
export const createAddress = (body) => Request().post('addresses', body);
export const updateAddress = (id, body) => Request().put('addresses/' + id, body);
export const deleteAddress = (id) => Request().delete('addresses/' + id);

export const createPaymentIntent = (body) => Request().post('create-payment-intent', body);

export const createOrder = (body) => Request().post('orders', body);
export const getOrders = () => Request().get("orders");
export const getOrder = (id) => Request().get('orders/' + id);

export const getDeliveries = () => Request().get("deliveries");
export const getDelivery = (id) => Request().get("deliveries/" + id);
export const updateDelivery = (id, body) => Request().put('deliveries/' + id, body);
export const updateOrder = (id, body) => Request().put('orders/' + id, body);