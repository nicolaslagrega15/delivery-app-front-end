import axios from 'axios';

const REACT_APP_API_URL = process.env.REACT_APP_API_URL;
const REACT_APP_STORAGE_USER_TOKEN = process.env.REACT_APP_STORED_USER_TOKEN;

const Request = () => {
  const tokenUser = localStorage.getItem(REACT_APP_STORAGE_USER_TOKEN);
  return axios.create({
    baseURL: REACT_APP_API_URL,
    headers: {
      ...(tokenUser ? { Authorization: ("Bearer " + tokenUser) } : {})
    }
  });
};

export default Request;
