import React, { useState, useEffect } from 'react'
import { Col, Container, Row, Button } from "react-bootstrap";
import { getAddressesByCustomer, deleteAddress } from "../api/requests";

import AddressForm from '../components/addresses/AddressForm';
import AddressTemplate from '../components/addresses/AddressTemplate'
import UserAddresses from '../components/addresses/UserAddresses'

import Loading from '../components/utils/Loading'

const Addresses = () => {
    const [addresses, setAddresses] = useState(null)
    const [showForm, setShowForm] = useState(true)
    const [addressUpdate, setAddressUpdate] = useState(null)
    const [addressSelected, setAddressSelected] = useState(null)

    async function refreshAddresses() {
        try {
            const { data } = await getAddressesByCustomer()
            if (data) setAddresses(data)
        } catch (error) {
        }
    }

    async function deleteAddresses(id) {
        try {
            const { data } = await deleteAddress(id)
            if (data) await refreshAddresses()
        } catch (error) {
        }
    }

    async function updateAddresses(address) {
        setShowForm(true)
        setAddressUpdate(address)
    }

    useEffect(() => {
        if (addresses && addresses.length > 0) setShowForm(false)
        else setShowForm(true)
    }, [addresses])

    useEffect(() => {
        refreshAddresses()

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    
    return (
        <Container fluid >
            <Row className="pl-4 pr-4 pt-4">
                <Col className="mb-4">
                    <h1>Mis direcciones</h1>
                </Col>
            </Row>
            <Row>
                <Col></Col>
                <Col xs={12} lg={7} className="px-3 mb-3">
                    {addressSelected ?
                        <>
                            <div className="border">
                                <h2 className="p-3 m-0 text-left bg-dark text-light font-weight-bold" style={{ fontSize: "1rem" }} >DIRECCIÓN SELECCIONADA</h2>
                                <div className="p-3">
                                    <AddressTemplate address={addressSelected} />
                                    <Button className="mt-4 rounded-0" variant="dark" onClick={setAddressSelected(null)}>Cambiar dirección</Button>
                                </div>
                            </div>
                            <Col xs={12} className="mt-3 p-0 border">
                                <h2 className="p-3 m-0 text-left bg-dark text-light font-weight-bold" style={{ fontSize: "1rem" }} >MÉTODO DE PAGO</h2>

                            </Col>
                        </>
                        :
                        <>
                            {addresses ?
                                <>
                                    {showForm ?
                                        <>
                                            <h2 className="p-3 m-0 text-left bg-dark text-light font-weight-bold" style={{ fontSize: "1rem" }} >AÑADIR DIRECCIÓN</h2>
                                            <AddressForm refreshAddresses={refreshAddresses} addressUpdate={addressUpdate} setAddressUpdate={setAddressUpdate} setShowForm={setShowForm} />
                                        </>
                                        :
                                        <>
                                            <h2 className="p-3 m-0 text-left bg-dark text-light font-weight-bold" style={{ fontSize: "1rem" }} >MIS DIRECCIONES</h2>
                                            <UserAddresses refreshAddresses={refreshAddresses} updateAddresses={updateAddresses} deleteAddresses={deleteAddresses} addresses={addresses} setAddressSelected={setAddressSelected} />
                                            <Button className="mt-4 rounded-0 mx-auto d-flex justify-content-center" variant="dark" onClick={() => { setShowForm(true); window.scrollTo({ top: 0, behavior: 'smooth' }) }} >Añadir nueva dirección</Button>
                                        </>
                                    }
                                </>
                                :
                                <Loading />
                            }
                        </>
                    }
                </Col>
                <Col></Col>
            </Row>
        </Container>
    )
}
export default Addresses