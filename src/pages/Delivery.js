import moment from "moment";
import React, { useEffect, useState } from "react"
import { Col, Container, Row, Table } from 'react-bootstrap';
import { useParams } from "react-router-dom";
import { getDelivery } from "../api/requests";
import { setColorState } from "../components/utils/colorState";

import Loading from "../components/utils/Loading";

const Delivery = () => {
    let { id } = useParams();

    const [delivery, setDelivery] = useState(null)

    useEffect(() => {

        (async function () {
            try {
                const { data } = await getDelivery(id)
                setDelivery(data)
            } catch (error) {
            }


        })();

    }, [])

    function calculateNumberProductsByOrder(order) {
        let total = 0;
        order.orderproducts.forEach(product => total += product.quantity)
        return total
    }

    if (!delivery) return <Loading />

    return (
        <Container fluid >
            <Row className="pl-4 pr-4 pt-4">
                <Col>
                    <h1>Entrega</h1>
                </Col>
            </Row>
            <Row>
                <Col></Col>
                <Col md={10} className="p-0" >
                    {delivery ?
                        <>
                            <Table striped bordered hover size="sm" className="mt-4">
                                <thead>
                                    <tr>
                                        <th>Referencia</th>
                                        <th>Creación</th>
                                        <th>Repartos</th>

                                    </tr>
                                </thead>
                                <tbody className="text-center">
                                    <tr key={delivery.id}>
                                        <td>{delivery.id}</td>
                                        {<td>{moment(delivery.created_at).format('DD-MM-YYYY')}</td>}
                                        <td>{delivery.orders.length}</td>
                                    </tr>
                                </tbody>
                            </Table>
                            <Table striped bordered hover size="sm" className="mt-4">
                                <thead>
                                    <tr>
                                        <th>Referencia</th>
                                        <th>dirección</th>
                                        <th>Total</th>
                                        <th>Bultos</th>
                                        <th>Estado</th>
                                    </tr>
                                </thead>
                                <tbody className="text-center">

                                    {delivery.orders.map(order =>
                                        <tr key={order.id}>
                                            <td>{order.id}</td>
                                            <td>{order.address.companyName} - {order.address.address} , {order.address.secondAddress} </td>
                                            <td>{order.amount.toFixed(2)}€</td>
                                            <td>{calculateNumberProductsByOrder(order)}</td>
                                            <td><span className={"p-2 badge badge-" + setColorState(order.status.toLowerCase()) + " text-wrap"}>{order.status}</span></td>
                                        </tr>
                                    )}
                                </tbody>
                            </Table>
                        </>
                        :
                        <Loading />
                    }
                </Col>
                <Col></Col>
            </Row>
        </Container>
    )
}

export default Delivery