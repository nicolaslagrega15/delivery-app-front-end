import React, { useState, useContext } from 'react';
import './../styles/login.css'
import { useHistory, NavLink } from "react-router-dom";

import { Container, Row, Col, Form, Button, Spinner, Toast, InputGroup } from 'react-bootstrap'
import GoogleButton from 'react-google-button'

import { UserContext } from "../wrappers/UserWrapper";

import userIcon from './../media/img/icons/user.svg'
import lockIcon from './../media/img/icons/lock.svg'

const SignUp = () => {
    const apiUrl = process.env.REACT_APP_API_URL
    const { registerUser } = useContext(UserContext)

    const [loading, setLoadign] = useState(false)
    const [showToast, setShowToast] = useState(false)

    let history = useHistory();

    const [validated, setValidated] = useState(false);

    const handleSubmit = async (e) => {

        e.preventDefault();
        e.stopPropagation();
        const form = e.currentTarget;

        if (form.checkValidity() === false) {
            //Set valid if the passwords match
            if (e.target.password.value === e.target.confirmPassword.value) form.formGroupPassword2.setCustomValidity("")
            else setShowToast("Las contraseñas tienen que coincidir")
        } else {
            const email = e.target.email.value
            const name = e.target.name.value
            const surnames = e.target.surnames.value
            const password = e.target.password.value
            const confirmPassword = e.target.confirmPassword.value

            //PONER MIN LENGTH ETC

            if (password !== confirmPassword) form.formGroupPassword2.setCustomValidity("Passwords Don't Match")
            else {
                setLoadign(true)
                const user = await registerUser({ email, name, surnames, password })
                if (user.email) {
                    history.push("/confirm-your-email");
                } else {
                    setLoadign(false)
                    setShowToast(user)
                }
            }
        }
        setValidated(true);
    }

    return (
        <Container fluid className="bg-light h-100 ">
            <Row>
                <Col xs={12} className="d-flex justify-content-center p-0 p-sm-5">

                    <Form noValidate validated={validated} onSubmit={handleSubmit} onClick={() => setShowToast(false)} className="form-sing-in pt-3 pt-sm-5 pl-5 pr-5 pb-5 bg-white text-center">
                        <h1 className="mb-5 text-center font-weight-bolder">Sign up</h1>
                        <Form.Group controlId="formGroupName">
                            <InputGroup className="border rounded-pill px-3">
                                <img src={userIcon} width="15px"></img>
                                <Form.Control required className="border-0 " type="text" name="name" placeholder="Nombre" />
                            </InputGroup>
                        </Form.Group>
                        <Form.Group controlId="formGroupSurNames">
                            <InputGroup className="border rounded-pill px-3">
                                <img src={userIcon} width="15px"></img>
                                <Form.Control required className="border-0 " type="text" name="surnames" placeholder="Apellidos" />
                            </InputGroup>
                        </Form.Group>
                        <Form.Group controlId="formGroupEmail">
                            <InputGroup className="border rounded-pill px-3">
                                <img src={userIcon} width="15px"></img>
                                <Form.Control required className="border-0 " type="email" name="email" placeholder="Email"  autoComplete="username" />
                            </InputGroup>
                        </Form.Group>
                        <Form.Group controlId="formGroupPassword1">
                            <InputGroup className="border rounded-pill px-3">
                                <img src={lockIcon} width="15px"></img>
                                <Form.Control required className="border-0 " type="password" name="password" placeholder="Contraseña" autoComplete="new-password" />
                            </InputGroup>
                        </Form.Group>
                        <Form.Group controlId="formGroupPassword2">
                            <InputGroup className="border rounded-pill px-3 mb-5">
                                <img src={lockIcon} width="15px"></img>
                                <Form.Control required className="border-0 " type="password" name="confirmPassword" placeholder="Confirmar contraseña" autoComplete="new-password" />
                            </InputGroup>
                        </Form.Group>
                        {loading ?
                            <Button variant="secondary" size="lg" className="rounded-pill mb-5">
                                <Spinner
                                    as="span"
                                    animation="border"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />
                                 Loading...
                            </Button>
                            :
                            <Button variant="secondary" type="submit" size="lg" className="rounded-pill px-5 font-weight-bolder mb-5">
                                Siguiente paso
                            </Button>
                        }
                        <ToastError showToast={showToast} />
                        <div className="d-flex justify-content-center mb-5">
                            <hr className="w-25 mx-4 " />
                            <span className="d-inline text-black-50 font-weight-bold">o</span>
                            <hr className="w-25 mx-4" />
                        </div>
                        <div className="d-flex justify-content-center mb-5">
                            <GoogleButton label="Regístrate con google" className="d-block" onClick={() => (window.location = apiUrl + "/connect/google")} />
                        </div>
                        <p>¿Tienes una cuenta? <NavLink to="/sign-in"><span className="font-weight-bolder text-secondary" role="button">Entrar</span></NavLink></p>
                        <p className="text-center font-weight-lighter" style={{ fontSize: "0.6rem" }}>Al continuar, aceptas los <span className="font-weight-bold" role="button">Términos del servicio</span> y la <span className="font-weight-bold" role="button">Política de privacidad.</span></p>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

const ToastError = ({ showToast }) => {
    return (
        <Toast className="mt-3" show={Boolean(showToast)}>
            <Toast.Body className="text-danger">{showToast}</Toast.Body>
        </Toast>
    )
}





export default SignUp;