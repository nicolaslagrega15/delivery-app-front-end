import React, { useContext, useEffect, useState } from "react";
import { useParams, Redirect, useHistory } from 'react-router-dom'
import moment from 'moment'
import { Container, Row, Col, Table, Button } from 'react-bootstrap';

import { getOrder, getProductById } from '../api/requests'
import { setColorState } from '../components/utils/colorState'
import Loading from "../components/utils/Loading";

import productDefault from '../media/img/default-images/product-default.jpg'
import { CartContext } from "../wrappers/CartWrapper";

const Order = () => {
    let { id } = useParams();
    const history = useHistory();
    const [order, setOrder] = useState(null)
    const [products, setProducts] = useState(null)

    const { addItem } = useContext(CartContext)

    useEffect(() => {

        (async function () {
            try {
                const { data } = await getOrder(id)


                const productsData = await Promise.all(
                    data.orderproducts.map(async product => {
                        const productData = await getProductById(product.product)
                        return productData.data
                    })
                )

                const productsMerged = []
                data.orderproducts.forEach(item => {
                    productsMerged.push({
                        ...item,
                        ...(productsData.find((itemInner) => itemInner.id === item.product))
                    })
                });

                data.orderproducts = productsMerged
                setOrder(data)

            } catch (error) {
                history.push("/my-orders");
            }
        })();

    }, [id])

    async function repeatOrder() {
        order.orderproducts.forEach(async product => await addItem(product.product, product.quantity))
       // history.push("/checkout");
    }

    if (!order) return <Loading />

    return (
        <Container fluid>
            <Row>
                <Col className="p-3">
                    <h1>Resumen de pedido - {order.id}</h1>
                </Col>
            </Row>
            <Row>
                <Col></Col>
                <Col ms={10} md={8}>
                    <Table striped bordered hover size="sm">
                        <thead>
                            <tr>
                                <th>Ref</th>
                                <th>Fecha</th>
                                <th>Total</th>
                                <th>Estado</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody className="text-center">
                            <tr>
                                <td>{order.id}</td>
                                {<td>{moment(order.created_at).format('DD-MM-YYYY')}</td>}
                                <td>{order.amount.toFixed(2)}€</td>
                                <td className="text-center" ><span className={"p-2 badge badge-" + setColorState(order.status.toLowerCase()) + " text-wrap"}>{order.status}</span></td>
                                <td className="text-center"><Button variant="warning" size="sm" onClick={repeatOrder}>REPETIR PEDIDO</Button></td>
                            </tr>
                        </tbody>
                    </Table>
                </Col>

                <Col></Col>
            </Row>
            <Row>
                <Col></Col>
                <Col xs={12} md={8} className="mt-3">
                    <div className="border">
                        <h2 className="p-3 m-0 text-left bg-dark text-light font-weight-bold" style={{ fontSize: "1rem" }} >DIRECCIÓN DE ENTREGA</h2>
                        <div className="p-3">
                            <AddressTemplate address={order.address} />
                        </div>
                    </div>
                </Col>
                {/*   <Col xs={12} md={5} xl={4} className="mt-3">
                    <div className="border">
                        <h2 className="p-3 m-0 text-left bg-dark text-light font-weight-bold" style={{ fontSize: "1rem" }} >INFORMACIÓN DE PAGO</h2>
                        <div className="p-3">
                            <PaymentTemplate paymentMethod={order.paymentMethod} />
                        </div>
                    </div>
                </Col> */}
                <Col></Col>
            </Row>
            <ProductsResume products={order.orderproducts} />
        </Container >
    )
}

const AddressTemplate = ({ address }) => {
    return (
        <>
            <span className="font-weight-bold d-block">{address.name} {address.surnames}</span>
            <span className="d-block">{address.address},  {address.secondAddress && address.secondAddress}</span>
            <span className="d-block">{address.country}, {address.province}, {address.city}, {address.postalCode}</span>
            <span className="d-block">{address.phoneNumber}</span>
        </>
    )
}

const PaymentTemplate = ({ paymentMethod }) => {
    return (
        <>
            <span className="font-weight-bold d-block">Método de pago: {paymentMethod}</span>
            {/*  <span className="d-block">Estado del pago: <span className={"p-2 badge badge-" + setColorState(payment.status.toLowerCase()) + " text-wrap"}>{payment.status}</span></span> */}

        </>
    )
}

const ProductsResume = ({ products }) => {
    const API_URL = process.env.REACT_APP_API_URL;

    return (
        <Row className="mt-3">
            <Col></Col>
            <Col ms={10} md={8}>

                <div className="products-resume p-3 border-right border-left border-bottom  border-top">
                    {products.map((item, index) => (
                        <Row key={index} className="mb-2 py-2 px-3  border-bottom">
                            <Col xs={4} lg={4} key={item.id} style={{ backgroundImage: `url(${item.image[0] ? API_URL + item.image[0].url : productDefault})`, backgroundSize: "cover", backgroundPosition: "center" }}></Col>
                            <Col xs={8} lg={8} className="p-0 pl-4 mb-2">
                                {/* <span className="text-left d-block" style={{ fontWeight: "600", color: "#000", fontSize: "0.8rem" }}>{item.name}</span> */}
                                <span className="text-left d-block" style={{ fontWeight: "600", color: "#000", fontSize: "0.8rem" }}>{item.name}</span>
                                <span className="text-left d-block text-secondary" style={{ fontSize: "0.9rem" }}>Cant.:{item.quantity}  -  €{item.price}</span>
                                <span className="text-left d-block text-dark " style={{ fontSize: "0.8rem" }}>€{item.quantity * item.price}</span>
                            </Col>
                        </Row>
                    ))}
                </div>
            </Col>
            <Col></Col>
        </Row>

    )
}


export default Order