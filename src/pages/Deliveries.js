import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { Col, Container, Row, Table, Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { getDeliveries } from '../api/requests';
import { setColorState } from '../components/utils/colorState';
import Loading from '../components/utils/Loading';

const Deliveries = () => {

    const [deliveries, setDeliveries] = useState(null)

    useEffect(() => {

        (async function () {
            try {
                const { data } = await getDeliveries()
                setDeliveries(data)
            } catch (error) {
            }
        })();

    }, [])

    return (
        <Container fluid >
            <Row className="pl-4 pr-4 pt-4">
                <Col>
                    <h1>Mis entregas</h1>
                </Col>
            </Row>
            <Row>
                <Col></Col>
                <Col md={10} className="p-0" >
                    {deliveries ?
                        <Table striped bordered hover size="sm" className="mt-4">
                            <thead>
                                <tr>
                                    <th>Referencia</th>
                                    <th>Fecha</th>
                                    <th>Total</th>
                                    <th>Estado</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody className="text-center">
                                {deliveries.map(delivery =>
                                    <tr key={delivery.id}>
                                        <td>{delivery.id}</td>
                                        {<td>{moment(delivery.created_at).format('DD-MM-YYYY')}</td>}
                                        <td>{delivery.orders.length}</td>
                                        <td><span className={"p-2 badge badge-" + setColorState(delivery.status.toLowerCase()) + " text-wrap"}>{delivery.status}</span></td>
                                        {delivery.status === "pending" &&
                                            <td>
                                                <NavLink to={"/map/" + delivery.id}>
                                                    <Button variant="secondary" size="sm">INICIAR</Button>
                                                </NavLink>
                                            </td>
                                        }
                                        {delivery.status === "transit" &&
                                            <td>
                                                <NavLink to={"/map/" + delivery.id}>
                                                    <Button variant="secondary" size="sm">CONTINUAR</Button>
                                                </NavLink>
                                            </td>
                                        }
                                        {delivery.status === "finalized" &&
                                            <td>
                                                <NavLink to={"/my-deliveries/delivery/" + delivery.id}>
                                                    <Button variant="secondary" size="sm">DETALLES</Button>
                                                </NavLink>
                                            </td>
                                        }
                                    </tr>
                                )}
                            </tbody>
                        </Table>
                        :
                        <Loading />
                    }
                </Col>
                <Col></Col>
            </Row>
        </Container>
    )

}


export default Deliveries