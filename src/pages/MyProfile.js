import React, { useContext } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { UserContext } from "../wrappers/UserWrapper";

import ButtonCategory from "../components/myProfile/ButtonCategory"

const MyProfile = () => {
    const { user } = useContext(UserContext)
    return (
        <Container fluid >
            <Row className="pl-4 pr-4 pt-4">
                <Col>
                    <h1>Mi cuenta</h1>
                </Col>
            </Row>
            <Row className="p-4">
                {user.role.type === "authenticated" ? <UserOptions /> : <DeliveryManOptions />}
            </Row>
        </Container>
    )
}

const UserOptions = () => {
    return (
        <>
            <ButtonCategory title={"Mis pedidos"} link={"/my-orders"} text={"Realizar el seguimiento, devolver un producto o repetir compras anteriores"} />
            <ButtonCategory title={"Direcciones"} link={"/my-addresses"} text={"Editar direcciones y preferencias de envío para pedidos"} />
            <ButtonCategory title={"Inicio de sesión y seguridad"} link={"/my-info"} text={"Editar inicio de sesión, nombre y número de teléfono móvil"} />
        </>
    )
}

const DeliveryManOptions = () => {
    return (
        <>
            <ButtonCategory title={"Mis entregas"} link={"/my-deliveries"} text={"Realizar entregas o visualizar entregas anteriores"} />
            <ButtonCategory title={"Inicio de sesión y seguridad"} link={"/my-info"} text={"Editar inicio de sesión, nombre y número de teléfono móvil"} />
        </>
    )
}

export default MyProfile