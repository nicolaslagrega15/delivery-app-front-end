import React, { useState, useContext } from 'react';
import './../styles/login.css'
import { useHistory, NavLink } from "react-router-dom";

import { Container, Row, Col, Form, Button, Spinner, Toast, InputGroup } from 'react-bootstrap'
import GoogleButton from 'react-google-button'

import { UserContext } from "../wrappers/UserWrapper";

import userIcon from './../media/img/icons/user.svg'
import lockIcon from './../media/img/icons/lock.svg'

const Login = () => {
    const apiUrl = process.env.REACT_APP_API_URL
    const { loginLocal } = useContext(UserContext)

    const [loading, setLoadign] = useState(false)
    const [showToast, setShowToast] = useState(false)

    let history = useHistory();

    const onSubmit = async (e) => {
        e.preventDefault();

        setLoadign(true)

        const identifier = e.target.identifier.value
        const password = e.target.password.value

        const user = await loginLocal({ identifier, password })
        if (user.email) {
            if (user.role.type === "authenticated") history.push('/store')
            else history.push('/my-profile')
        } else {
            setLoadign(false)
            setShowToast(user)
        }
    }

    return (
        <Container fluid className="bg-light h-100 ">
            <Row>
                <Col xs={12} className="d-flex justify-content-center p-0 p-sm-5">
                    <Form className="form-sing-in pt-3 pt-sm-5 pl-5 pr-5 pb-5 bg-white text-center" onSubmit={onSubmit} onClick={() => setShowToast(false)}>
                        <h1 className="mb-5 text-center font-weight-bolder">Iniciar sesión</h1>
                        <Form.Group controlId="formGroupEmail">
                            <InputGroup className="border rounded-pill px-3">
                                <img src={userIcon} width="15px" alt="user icon"></img>
                                <Form.Control className="border-0 " type="email" name="identifier" placeholder="Email" autoComplete="username" defaultValue="nicolaslagrega15@gmail.com" />
                            </InputGroup>
                        </Form.Group>
                        <Form.Group controlId="formGroupPassword">
                            <InputGroup className="border rounded-pill px-3">
                                <img src={lockIcon} width="15px" alt="password icon"></img>
                                <Form.Control className="border-0 " type="password" name="password" placeholder="Contraseña" autoComplete="current-password" defaultValue="123456" />
                            </InputGroup>
                        </Form.Group>
                        <span className="mb-5 text-secondary d-block text-right" role="button">¿Has olvidado la contraseña?</span>
                        {loading ?
                            <Button variant="secondary" size="lg" className="rounded-pill mb-5">
                                <Spinner
                                    as="span"
                                    animation="border"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />
                                Loading...
                            </Button>
                            :
                            <Button variant="secondary" type="submit" size="lg" className="rounded-pill px-5 font-weight-bolder mb-5">
                                Iniciar sesión
                            </Button>
                        }
                        <ToastError showToast={showToast} />
                        <div className="d-flex justify-content-center mb-5">
                            <hr className="w-25 mx-4 " />
                            <span className="d-inline text-black-50 font-weight-bold">o</span>
                            <hr className="w-25 mx-4" />
                        </div>
                        <div className="d-flex justify-content-center mb-5">
                            <GoogleButton label="Inciar sesión con google" className="d-block" onClick={() => (window.location = apiUrl + "/connect/google")} />
                        </div>
                        <p>¿No tienes una cuenta? <NavLink to="/sign-up"><span className="font-weight-bolder text-secondary" role="button">Regístrate</span></NavLink></p>
                        <p className="text-center font-weight-lighter" style={{ fontSize: "0.6rem" }}>Al continuar, aceptas los <span className="font-weight-bold" role="button">Términos del servicio</span> y la <span className="font-weight-bold" role="button">Política de privacidad.</span></p>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}


const ToastError = ({ showToast }) => {
    return (
        <Toast className="mt-3" show={Boolean(showToast)}>
            <Toast.Body className="text-danger">{showToast}</Toast.Body>
        </Toast>
    )
}

export default Login;