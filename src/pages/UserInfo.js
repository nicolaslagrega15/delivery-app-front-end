import React, { useContext } from 'react'
import { Col, Container, Row, Form, Button} from "react-bootstrap";
import { UserContext } from '../wrappers/UserWrapper'

const UserInfo = () => {

    const { user } = useContext(UserContext)
    return (
        <Container fluid >
            <Row className="pl-4 pr-4 pt-4">
                <Col className="mb-4">
                    <h1>Inicio de sesión y seguridad</h1>
                </Col>
            </Row>
            <Row>
                <Col />
                <Col md={7}>
                    <Form noValidate /* validated={validated} onSubmit={handleSubmit} */ className="border p-3 w-auto">
                        <Form.Row>
                            <Form.Group as={Col} md="6" controlId="user">
                                <Form.Control
                                    required
                                    type="text"
                                    placeholder="Usuario"
                                    defaultValue={user ? user.username : ""}
                                />
                                <Form.Control.Feedback type="invalid">Escribe tu nombre de usuario.</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group as={Col} md="6" controlId="email">
                                <Form.Control
                                    required
                                    type="text"
                                    placeholder="Email"
                                    defaultValue={user ? user.email : ""}
                                />
                                <Form.Control.Feedback type="invalid">Escribe tu email.</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group as={Col} md="6" controlId="phoneNumber">
                                <Form.Control type="number" placeholder="Número de teléfono" required /* defaultValue={addressUpdate ? addressUpdate.phoneNumber : ""} */ />
                                <Form.Control.Feedback type="invalid">
                                    Este campo es obligatorio.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Button className="rounded-0 m-1" type="submit" variant="dark" >CAMBIAR CONTRASEÑA</Button>
                        </Form.Row>
                        
                        {/* <Button className="rounded-0 m-1" type="submit" variant="dark" >{addressUpdate ? "Actualizar" : "Añadir"} dirección</Button>
                        <Button className="rounded-0 m-1" variant="danger" onClick={() => { setShowForm(false); setAddressUpdate(null); }} >Cancelar</Button> */}
                    </Form>
                </Col>
                <Col />
            </Row>
        </Container>
    )
}

export default UserInfo