import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { Container, Row, Col, Table, Button } from 'react-bootstrap';
import moment from 'moment'

import { getOrders } from "../api/requests";
import { setColorState } from '../components/utils/colorState'
import Loading from "../components/utils/Loading";

const Orders = () => {

    const [orders, setOrders] = useState(null)

    useEffect(() => {
        try {
            (async function () {
                const { data } = await getOrders()
                setOrders(data)
            })();
        } catch (error) {
        }

    }, [])

    return (
        <Container fluid>
            <Row>
                <Col></Col>
                <Col md={11} className="pt-4 pb-4" >
                    <h1>Mis pedidos</h1>
                </Col>
            </Row>
            <Row>
                <Col></Col>
                <Col md={10} className="p-0" >
                    {orders ?
                        <>
                            <Table striped bordered hover size="sm">
                                <thead>
                                    <tr>
                                        <th>Referencia</th>
                                        <th>Fecha</th>
                                        <th>Total</th>
                                        <th>Estado</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody className="text-center">
                                    {orders.map(order =>
                                        <tr key={order.id}>
                                            <td>{order.id}</td>
                                            {<td>{moment(order.created_at).format('DD-MM-YYYY')}</td>}
                                            <td>{order.amount.toFixed(2)}€</td>
                                            <td><span className={"p-2 badge badge-" + setColorState(order.status.toLowerCase()) + " text-wrap"}>{order.status}</span></td>
                                            <td>
                                                <NavLink to={"/my-orders/order/" + order.id}>
                                                    <Button variant="secondary" size="sm">DETALLES</Button>
                                                </NavLink>
                                            </td>
                                        </tr>
                                    )}
                                </tbody>
                            </Table>
                        </>
                        :
                        <Loading />
                    }
                </Col>
                <Col></Col>
            </Row>

        </Container>
    )
}

export default Orders