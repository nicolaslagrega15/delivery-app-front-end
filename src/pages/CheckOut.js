import React, { useState, useContext, useEffect } from 'react'
import { Container, Row, Col, Button } from 'react-bootstrap';

import { getAddressesByCustomer, deleteAddress, createOrder } from "../api/requests";

import { CartContext } from './../wrappers/CartWrapper'
import { UserContext } from '../wrappers/UserWrapper'

import { Redirect } from "react-router-dom";

import AddressTemplate from '../components/addresses/AddressTemplate'
import AddressForm from '../components/addresses/AddressForm';
import UserAddresses from '../components/addresses/UserAddresses';

import ProductsCart from '../components/checkOut/products/ProductsCart';

import PaymentMethods from '../components/checkOut/order/PaymentMethods';
import FinalizeOrder from '../components/checkOut/order/FinalizeOrder';
import OrderCompletedJumbotron from '../components/checkOut/order/OrderCompletedJumbotron.js';

import Loading from '../components/utils/Loading'

const CheckOut = () => {
    const [addresses, setAddresses] = useState(null)
    const [showForm, setShowForm] = useState(true)
    const [addressUpdate, setAddressUpdate] = useState(null)
    const [addressSelected, setAddressSelected] = useState(null)
    const [paymentMethod, setPaymentMethod] = useState(null)
    const [orderCompleted, setOrderCompeted] = useState(false)

    const { user } = useContext(UserContext)
    const { cartItems, cartItemsMerged, cleanCart } = useContext(CartContext)

    async function refreshAddresses() {
        try {
            const { data } = await getAddressesByCustomer()
            if (data) setAddresses(data)
        } catch (error) {
        }
    }

    async function deleteAddresses(id) {
        try {
            const { data } = await deleteAddress(id)
            if (data) await refreshAddresses()
        } catch (error) {
        }
    }

    async function updateAddresses(address) {
        setShowForm(true)
        setAddressUpdate(address)
    }

    useEffect(() => {
        if (addresses && addresses.length > 0) setShowForm(false)
        else setShowForm(true)
    }, [addresses])

    useEffect(() => {
        refreshAddresses()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    async function createOrderCustomer(paymentIntentId) {
        try {
            const { data } = await createOrder({ address: addressSelected.id, cartItems, paymentIntentId })
            if (data) setOrderCompeted(true); cleanCart();
        } catch (error) {
        }
    }

    if (cartItems.length == 0) return <Redirect push to="/store" />

    return (
        <Container fluid style={{ marginTop: "58px" }}>
            {orderCompleted ?
                <OrderCompletedJumbotron />
                :
                <React.Fragment>
                    <Row className="p-md-5">
                        <Col xs={1}>
                        </Col>
                        <Col xs={12} lg={11} className="mt-xs-5 mb-3">
                            <h1 style={{ fontSize: "1.6rem" }} className="font-weight-bold">PASAR POR CAJA</h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={1}></Col>
                        <Col xs={12} lg={7} className="px-3 mb-3">
                            {addressSelected ?
                                <React.Fragment>
                                    <div className="border">
                                        <h2 className="p-3 m-0 text-left bg-dark text-light font-weight-bold" style={{ fontSize: "1rem" }} >DIRECCIÓN SELECCIONADA</h2>
                                        <div className="p-3">
                                            <AddressTemplate address={addressSelected} />
                                            <Button className="mt-4 rounded-0" variant="dark" onClick={() => { setAddressSelected(null); setPaymentMethod(null) }}>Cambiar dirección</Button>
                                        </div>
                                    </div>
                                    <Col xs={12} className="mt-3 p-0 border">
                                        <h2 className="p-3 m-0 text-left bg-dark text-light font-weight-bold" style={{ fontSize: "1rem" }} >MÉTODO DE PAGO</h2>
                                        <PaymentMethods setPaymentMethod={setPaymentMethod} />
                                        {paymentMethod && <FinalizeOrder paymentMethod={paymentMethod} createOrderCustomer={createOrderCustomer} />}
                                    </Col>
                                </React.Fragment>
                                :
                                <React.Fragment>
                                    {addresses ?
                                        <React.Fragment>
                                            {showForm ?
                                                <React.Fragment>
                                                    <h2 className="p-3 m-0 text-left bg-dark text-light font-weight-bold" style={{ fontSize: "1rem" }} >AÑADIR DIRECCIÓN</h2>
                                                    <AddressForm refreshAddresses={refreshAddresses} addressUpdate={addressUpdate} setAddressUpdate={setAddressUpdate} setShowForm={setShowForm} />
                                                </React.Fragment>
                                                :
                                                <React.Fragment>
                                                    <h2 className="p-3 m-0 text-left bg-dark text-light font-weight-bold" style={{ fontSize: "1rem" }} >MIS DIRECCIONES</h2>
                                                    <UserAddresses refreshAddresses={refreshAddresses} updateAddresses={updateAddresses} deleteAddresses={deleteAddresses} addresses={addresses} setAddressSelected={setAddressSelected} />
                                                    <Button className="mt-4 rounded-0 mx-auto d-flex justify-content-center" variant="dark" onClick={() => { setShowForm(true); window.scrollTo({ top: 0, behavior: 'smooth' }) }} >Añadir nueva dirección</Button>
                                                </React.Fragment>
                                            }
                                        </React.Fragment>
                                        :
                                        <Loading />
                                    }
                                </React.Fragment>
                            }
                        </Col>
                        <ProductsCart />
                    </Row>
                </React.Fragment>
            }
        </Container >
    )
}

export default CheckOut