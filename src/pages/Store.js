import React, { useState, useEffect } from "react"
import { Col, Container, Row, Dropdown, Button } from 'react-bootstrap';

import { getProducts, getCategories, getTotalProducts } from "../api/requests";

import Products from '../components/store/Products'
import ScrollArrow from '../components/utils/ScrollArrow/ScrollArrow'

import filterIcon from '../media/img/icons/filter-icon.svg'
import flecha from '../media/img/icons/flecha.svg'

const Store = () => {

    const [products, setProducts] = useState(null)
    const [productsFilter, setProductsFilter] = useState(null)
    const [categories, setCategories] = useState(null)
    const [order, setOrder] = useState("Ordenar por")

    const [totalProducts, setTotalProducts] = useState(null)
    const [start, setStart] = useState(0)
    const [limit, setLimit] = useState(10)

    async function getProductsRequest() {
        try {
            const { data } = await getProducts(start, limit)
            if (products) { setProducts([...products, ...data]); setProductsFilter([...productsFilter, ...data]); }
            else { setProducts(data); setProductsFilter(data); }
            setStart((start + limit))
        } catch (error) {
        }
    }

    useEffect(() => {

        (async function () {

            try {
                const { data } = await getTotalProducts()
                setTotalProducts(data)
            } catch (error) {
            }

            getProductsRequest()

            try {
                const data = await getCategories()
                setCategories(data)
            } catch (error) {
            }
        })();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    async function getNextPageProducts() {
        setStart((start + limit))
        getProductsRequest()
    }

    function orderFeatured() {
        setOrder("Relevancia")
        setProductsFilter(productsFilter.sort((a, b) => a.id - b.id))
    }

    function orderUp() {
        setOrder("Precio: menor a mayor")
        setProductsFilter(productsFilter.sort((a, b) => a.price - b.price))
    }

    function orderDown() {
        setOrder("Precio: mayor a menor")
        setProductsFilter(productsFilter.sort((a, b) => b.price - a.price))
    }

    return (
        <Container fluid >
            <ScrollArrow />
            <Row className="d-flex justify-content-center">
                <Col className="d-flex justify-content-center p-3 border" xs={6} xl={5}>
                    <div className="w-100 d-flex justify-content-center" role="button">
                        <span className="text-dark align-self-center" >Filtar</span>
                        <img className="align-self-center" alt="filtro icono" src={filterIcon} width="25px" />
                    </div>
                </Col>
                <Col className="p-3 border" xs={6} xl={5} >
                    <OrderMenu order={order} orderFeatured={orderFeatured} orderUp={orderUp} orderDown={orderDown} />
                </Col>
            </Row>
            <Row className="d-flex justify-content-center">
                <Col xs={12} xl={10}>
                    <Products products={productsFilter} />
                </Col>
            </Row>
            { start < totalProducts &&
                <Row>
                    <Col className="text-center">
                        <Button variant="primary text-center rounded-pill my-4" size="lg" onClick={getNextPageProducts} >VER MÁS</Button>{' '}
                    </Col>
                </Row>
            }
        </Container >
    )
}

const OrderMenu = ({ order, orderFeatured, orderUp, orderDown }) => {
    return (
        <Dropdown className="d-flex justify-content-center">
            <Dropdown.Toggle className="text-dark" variant="transparent" size="sm" drop='down' id="dropdown-basic">
                {order}
            </Dropdown.Toggle>

            <Dropdown.Menu className=" text-secondary">
                <Dropdown.Item className onClick={orderFeatured} as="button">Relevancia</Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item className="p-1" onClick={orderUp} as="button"><img alt="user icon" src={flecha} width="25" className="pl-2 " style={{ transform: "rotate(-90deg)" }} />{''}menor a mayor</Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item className="p-1" onClick={orderDown} as="button"><img alt="user icon" src={flecha} width="25" className="pl-2 " style={{ transform: "rotate(90deg)" }} />{''}mayor a menor</Dropdown.Item>
            </Dropdown.Menu>
        </Dropdown>
    )
}



export default Store