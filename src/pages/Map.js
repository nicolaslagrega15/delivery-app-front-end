import React, { useEffect, useState } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';
import { getDelivery, getProductById, updateDelivery, updateOrder } from '../api/requests';
import Loading from '../components/utils/Loading';

import gpsIcon from '../media/img/icons/gps.svg'
import InfoMarkerOrder from '../components/map/InfoMarkerOrder';

const MapContainer = (props) => {

    let { id } = useParams();
    const history = useHistory();
    const [delivery, setDeleivery] = useState(null)
    const [coords, setCoords] = useState(null)
    const [currentPos, setCurrentPos] = useState(null)
    const [infoMarker, setInfoMarker] = useState(null)

    useEffect(() => {
        (function () {
            getCurrentDelivery()
        })();
    }, [id])

    useEffect(() => {
        getLocation()
    }, [])

    useEffect(() => {

        (async function () {
            if (delivery && delivery.status === "pending") updateStatusDelivery("transit")
            if (delivery && delivery.status === "finalized") history.push("/my-deliveries");
        })();

        if (delivery) {
            (async function () {
                try {

                    const newDelivery = {...delivery}
                    newDelivery.orders.forEach(async order => {
                        const productsData = await Promise.all(
                            order.orderproducts.map(async product => {
                                const productData = await getProductById(product.product)
                                return productData.data
                            })
                        )

                        const productsMerged = []
                        order.orderproducts.forEach(item => {
                            productsMerged.push({
                                ...item,
                                ...(productsData.find((itemInner) => itemInner.id === item.product))
                            })
                        });

                        order.orderproducts = productsMerged
                    })



                } catch (error) {
                    //history.push("/my-orders");
                }
            })();
        }


    }, [delivery])



    async function getCurrentDelivery() {
        try {
            const { data } = await getDelivery(id)
            setDeleivery(data)
            const newCoords = []
            data.orders.forEach(order => newCoords.push({ lat: parseFloat(order.address.lat), lng: parseFloat(order.address.lng) }))
            setCoords(newCoords)

        } catch (error) {
            history.push("/my-deliveries");
        }
    }

    async function updateStatusDelivery(newStatus) {
        try {
            const { data } = await updateDelivery(delivery.id, { status: newStatus })

            delivery.orders.forEach(async order => {

                if (order.status === "pending") {
                    const { data } = await updateOrder(order.id, { status: "transit" })
                }
            })
            getCurrentDelivery()
        } catch (error) {
        }
    }

    async function updateStatusOrder(id, newStatus) {
        try {
            const { data } = await updateOrder(id, { status: newStatus })
        } catch (error) {
        }

        try {
            const { data } = await getDelivery(delivery.id)
            setDeleivery(data)

            let finalizeDelivery = true
            data.orders.forEach(order => {
                if (order.status === "transit") finalizeDelivery = false
            })

            if (finalizeDelivery) {
                updateStatusDelivery("finalized")
            }
        } catch (error) {
            history.push("/my-deliveries");

        }
    }

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.watchPosition(function (position) {
                const pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                };
                setCurrentPos(pos)
            });
        }
    }

    const showInfoMarker = () => {
        if (infoMarker) setInfoMarker(null)
    }

    const onMarkerClick = (props, marker, order) => {
        setInfoMarker(order)
    }

    if (!delivery || !coords) return <Loading />

    return (
        <>
            <Map google={props.google} zoom={12} style={style} initialCenter={coords[0]}>

                {currentPos &&
                    <Marker
                        name={'Your position'}
                        position={currentPos}
                        icon={{
                            url: gpsIcon,
                            anchor: new props.google.maps.Point(32, 32),
                            scaledSize: new props.google.maps.Size(32, 32)
                        }} />
                }

                {delivery.orders.map(order =>
                    <Marker
                        key={order.id}
                        title={'Clik para más información'}
                        name={'SOMA'}
                        position={{ lat: order.address.lat, lng: order.address.lng }}
                        onClick={(props, marker) => onMarkerClick(props, marker, order)}>
                    </Marker>
                )}

            </Map >
            {infoMarker && <InfoMarkerOrder infoMarker={infoMarker} updateStatusOrder={updateStatusOrder} showInfoMarker={showInfoMarker} />}
        </>
    );
}

const style = {
    width: '100%',
    height: '100%',
    position: 'relative'
}

export default GoogleApiWrapper({
    apiKey: (process.env.REACT_APP_GOOGLE_MAPS_API)
})(MapContainer)