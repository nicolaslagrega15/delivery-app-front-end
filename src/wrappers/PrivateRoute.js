import React, { useState, useContext, useEffect } from 'react';
import { Redirect, Route, useHistory } from "react-router-dom";

import { UserContext } from "./UserWrapper";

const PrivateRoute = ({ component: Component, ...rest }) => {
    let history = useHistory();
    const { user, refreshUser } = useContext(UserContext);

    const [render, setRender] = useState(false)
    const [isLogged, setIsLogged] = useState(user || false)


    useEffect(() => {
        (async function () {
            if (!user) setIsLogged(await refreshUser())
            //if(!user.role.type === "authenticated") history.push('/')

            setRender(true)
        })();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <React.Fragment>
            {render &&
                <Route {...rest} render={props =>
                    isLogged ?
                        <>
                            {rest.role
                                ? rest.role === user.role.type
                                    ? <Component {...props} />
                                    : <Redirect to={"/"} />
                                : <Component {...props} />
                            }
                        </>
                        :
                        <Redirect to={"/sign-in"} />
                }
                />
            }
        </React.Fragment>
    )
}

export default PrivateRoute