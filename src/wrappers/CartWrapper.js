import React, { createContext, useState, useEffect, useMemo, useContext } from "react";

import { UserContext } from '../wrappers/UserWrapper';

import { getProductById } from "../api/requests"

export const CartContext = createContext()

const CartWrapper = ({ children }) => {

    const STORED_USER_CART = process.env.REACT_APP_STORED_USER_CART
    const cartLocalStorage = localStorage.getItem(STORED_USER_CART)

    const [showCart, setShowCart] = useState(false)
    const [cartItems, setCartItems] = useState([])
    const [cartItemsMerged, setCartItemsMerged] = useState(null)

    const { jwt } = useContext(UserContext)

    useEffect(() => {
        if (cartLocalStorage) setCartItems(JSON.parse(cartLocalStorage))
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const totalItems = useMemo(() => {
        let items = 0
        cartItems.forEach(item => items += item.quantity)
        return items
    }, [cartItems])

    const totalPrice = useMemo(() => {
        if (!cartItemsMerged) return 0
        let total = 0;
        cartItemsMerged.forEach(item => {
            total += (item.price * item.quantity)
        });
        return total.toFixed(2)
    }, [cartItemsMerged])

    useEffect(() => {
        if (jwt) {
            (async function () {
                const itemsPromise = []
                cartItems.forEach(cartItem => {
                    itemsPromise.push(getProductById(cartItem.id))
                });
                const itemsResponse = (await Promise.all(itemsPromise)).map(item => item.data)

                let merged = []
                cartItems.forEach((item) => {
                    merged.push({
                        ...item,
                        ...(itemsResponse.find((itemInner) => itemInner.id === item.id))
                    })
                });
                setCartItemsMerged(merged)
            })();
        }
    }, [jwt, cartItems])


    const displayCart = (action) => {
        setShowCart(action)
    }

    const addItem = (itemId, quantity) => {
        let cart = [...cartItems]
        let addItem = true

        cart.forEach((cartItem) => {
            if (cartItem.id === itemId) {
                cartItem.quantity = cartItem.quantity + quantity
                addItem = false
            }
        })

        if (addItem) {
            cart.push(
                {
                    "id": itemId,
                    "quantity": quantity
                }
            )
        }
        localStorage.setItem(STORED_USER_CART, JSON.stringify(cart))
        setCartItems(cart)
    }

    const removeItem = (idItemDeleted) => {
        const itemsRemove = cartItems.filter(item => item.id !== idItemDeleted)
        setCartItems(itemsRemove)
        localStorage.setItem(STORED_USER_CART, JSON.stringify(itemsRemove))
    }

    const sumItem = (IdItemSum) => {
        const sumItem = cartItems.map(item => {
            if (item.id === IdItemSum) item.quantity++
            return item
        })

        setCartItems(sumItem)
        localStorage.setItem(STORED_USER_CART, JSON.stringify(sumItem))
    }

    const resItem = (IdItemRes) => {
        const restItem = cartItems.map(item => {
            if (item.quantity === 1) return item
            if (item.id === IdItemRes) {
                item.quantity--
                return item
            }
            return item
        })
        setCartItems(restItem)
        localStorage.setItem(STORED_USER_CART, JSON.stringify(restItem))
    }

    function cleanCart() {
        setCartItems([])
        localStorage.removeItem(STORED_USER_CART)
    }

    return <CartContext.Provider value={{ showCart, totalItems, cartItems, cartItemsMerged, totalPrice, displayCart, addItem, removeItem, sumItem, resItem, cleanCart }} children={children} />;
}

export default CartWrapper