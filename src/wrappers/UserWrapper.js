import React, { createContext, useState, useEffect } from "react";
import { registerRequest, loginRequest, getUserMe, getAddressesByCustomer } from "../api/requests"

export const UserContext = createContext()

const UserWrapper = ({ children }) => {
    const STORED_USER_TOKEN = process.env.REACT_APP_STORED_USER_TOKEN;
    const storedJwt = localStorage.getItem(STORED_USER_TOKEN);

    const STORED_USER_CART = process.env.REACT_APP_STORED_USER_CART
    const cartLocalStorage = localStorage.getItem(STORED_USER_CART)

    const [user, setUser] = useState(null)
    const [jwt, setJwt] = useState(storedJwt || null)
    const [userAddresses, setUserAddresses] = useState(null)
    useEffect(() => {
        (async function () {
            //if (!user && jwt) await refreshUser()
            // if (jwt) await getAddressesCustomer()
        })();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [jwt])

    const registerUser = async (body) => {
        try {
            logout()
            const { email, name, surnames, password } = body
            if (email && name && surnames && password) {
                const username = email.split('@')[0];
                const { data } = await registerRequest({ ...body, username })
                return data.user
            }
        } catch (error) {
            const { response } = error
            if (response.data.data[0]) {
                const message = response.data.data[0].messages[0].id
                if (message === "Auth.form.error.email.taken") return "Cuenta ya existente con este email"

            }
            return "Error al registar su cuenta, si el problema persiste contacte con nosotros"
        }
    }

    const loginLocal = async (body) => { 
        try {
            logout()
            const { data } = await loginRequest(body)
            if (data.user) {
                setUser(data.user);
                setJwt(data.jwt)
                localStorage.setItem(STORED_USER_TOKEN, data.jwt);
                return data.user;
            }
        } catch (error) {
            const { response } = error;
            const message = response.data.data[0].messages[0].id
            if (message === "Auth.form.error.confirmed") return "Email no confirmado, revise su correo electrónico"
            if (message === "Auth.form.error.invalid") return "Lo sentimos, no existe ningún usuario con ese email y contraseña"
            return "Error al iniciar sesión, si el problema persiste contacte con nosotros"
        }
    }

    const loginGoogle = async (data) => {
        if (data) {
            setUser(data.user);
            setJwt(data.jwt)
            localStorage.setItem(STORED_USER_TOKEN, data.jwt);
        }
    }

    const refreshUser = async () => {
        try {
            const { data } = await getUserMe()
            setUser(data)
            if (data) {
                return true
            }
        } catch (error) {
            logout()
            return false
        }
    }

    const logout = () => {
        localStorage.removeItem(STORED_USER_TOKEN);
        localStorage.removeItem(cartLocalStorage);
        setUser(null)
        setJwt(null)
    }

    const getAddressesCustomer = async () => {
        try {
            const { data } = await getAddressesByCustomer()
            if (data) setUserAddresses(data)
        } catch (error) {
        }
    }

    return <UserContext.Provider value={{ user, jwt, userAddresses, loginLocal, loginGoogle, registerUser, refreshUser, getAddressesCustomer, logout }} children={children} />;
}

export default UserWrapper